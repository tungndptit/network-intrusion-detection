# -*- coding: utf-8 -*-


import config
from utils import data_convert

accept_sample = set(['ftp'])

def load_train_data():
    features = list()
    labels = list()
    with open(config.FilePath.RAW_FILE_DATA, 'r') as fin:
        for line in fin.xreadlines():
            tokens = line.split(',')
            if tokens[2] in accept_sample:
                feature = list()
                for i, token in enumerate(tokens):
                    if i != 2 and i != len(tokens)-1:
                        feature.append(token)
                features.append(feature)
                labels.append(tokens[-1])
        fin.close()
    print 'Load data INFO: feature dimensions ' + str(len(features)) + 'x' + str(len(features[0])) +\
        ' and labels dimensions ' + str(len(labels)) + 'x1'
    return features, labels

def load_prediction_data():
    features = list()
    with open(config.FilePath.RAW_FILE_PREDICT, 'r') as fin:
        for line in fin.readlines():
            tokens = line.split(',')
            if tokens[2] in accept_sample:
                feature = list()
                for i, token in enumerate(tokens):
                    if i != 2 and i != len(tokens)-1:
                        feature.append(token)
                features.append(feature)
        fin.close()
    print 'Load data INFO: feature dimensions ' + str(len(features)) + 'x' + str(len(features[0]))
    return features


def split_data(features, labels, percent_split):
    features, labels = data_convert.shuffle_pair_data(data=features, labels=labels)
    po = int(len(features) * percent_split)
    return features[:po], labels[:po], features[po:], labels[po:]


