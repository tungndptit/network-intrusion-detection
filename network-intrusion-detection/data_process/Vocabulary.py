# -*- coding: utf-8 -*-

import re
import sys
from utils import imath


class Vocabulary(object):

    def __init__(self, features, labels):
        self.__f_dict = {}
        self.__l_dict = {}
        self.__feature = '_feature_'
        self.__label = '_label_'
        self.__add_data_as_feature(features)
        self.__add_data_as_label(labels)

    def __add_data_as_feature(self, data_2d):
        for sample in data_2d:
            for i, f in enumerate(sample):
                try:
                    float(f)
                except ValueError:
                    key = str(i) + self.__feature + str(f)
                    if key not in self.__f_dict:
                        self.__f_dict[key] = len(self.__f_dict)

    def __add_data_as_label(self, labels):
        temps = {}
        for label in labels:
            if label not in temps:
                temps[label] = len(temps)
        for temp in temps:
            key = self.__label + temp
            changes = [0] * len(temps)
            changes[temps[temp]] = 1
            self.__l_dict[key] = changes

    def convert_features(self, features):
        new_features = list()
        for sample in features:
            feature = list()
            for i, f in enumerate(sample):
                try:
                    float(f)
                    feature.append(f)
                except ValueError:
                    key = str(i) + self.__feature + str(f)
                    feature.append(self.__f_dict[key])
            new_features.append(feature)
        return new_features

    def convert_labels(self, labels):
        new_labels = list()
        for label in labels:
            key = self.__label + label
            new_labels.append(self.__l_dict[key])
        return new_labels

    def show_dict(self):
        print '*' * 10 + 'feature' + '*' * 10
        for i in self.__f_dict:
            print i, self.__f_dict[i]
        print '*' * 10 + 'label' + '*' * 10
        for i in self.__l_dict:
            print i, self.__l_dict[i]

    def get_label_dim(self):
        return len(self.__l_dict)

    def find_labels(self, one_hot_vectors):
        labels = list()
        for vec in one_hot_vectors:
            labels.append(self.find_label(vec))
        return labels

    def find_label(self, one_hot_vector):
        best = ''
        distance = sys.maxint * 0.99
        for i in self.__l_dict:
            dis = imath.euclidean_distance(self.__l_dict[i], one_hot_vector)
            if dis < distance:
                # best = re.sub(str(i), self.__label, '')
                best = str(i).replace(self.__label, '')
                # print dis, self.__l_dict[i], 'best = ', best
                distance = dis
        return best

# v = Vocabulary()
# v.add_data_as_feature([[1.3, 'abc'], [2, 'x']])
# print v.convert_features([[1.3, 'abc'], [1.3, 'abc'], [5, 'x']])
# v.show_dict()
# v.add_data_as_label(['l 1', 'l 1', 'l 2', 'l 1', 'l 3'])
# print v.convert_labels(['l 3', 'l 1'])

