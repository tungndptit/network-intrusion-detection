# -*- coding: utf-8 -*-
import os


class FilePath(object):
    PATH_ROOT = os.path.dirname(os.getcwd())
    DATA = os.path.join(PATH_ROOT, 'data') + '/'
    # RAW_FILE_DATA = DATA + 'kddcup.data_10_percent_corrected'
    RAW_FILE_DATA = DATA + 'kddcup.data.corrected'

    FILTER_FILE_DATA = RAW_FILE_DATA + '.ftp'
    VOCABULARY = DATA + 'vocabulary.bin'

    RAW_FILE_PREDICT = DATA + 'predict.txt'
    RESULT_FILE_PREDICT = DATA + 'result_predict.txt'
