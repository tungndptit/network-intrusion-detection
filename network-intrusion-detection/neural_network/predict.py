#! /usr/bin/env python
# -*- coding: utf-8 -*-


import cPickle as pickle
import config
from data_process import data_loader
from neural_network import nn_model


def predict():
    with open(config.FilePath.VOCABULARY, 'rb') as fp:
        vob = pickle.load(fp)
        fp.close()
    raw_features = data_loader.load_prediction_data()
    features = vob.convert_features(raw_features)

    # vob.show_dict()

    mlp_config = nn_model.MLPCpnfig()
    mlp_config.LABEL_DIM = vob.get_label_dim()
    model = nn_model.MLPModel(mlp_config=mlp_config)
    output = model.predict_one(features=features)

    predict_labels = vob.find_labels(output)

    with open(config.FilePath.RESULT_FILE_PREDICT, 'w') as ft:
        for f, l in zip(raw_features, predict_labels):
            ft.write(','.join(f) + '--->' + str(l))
        ft.close()

    pass

