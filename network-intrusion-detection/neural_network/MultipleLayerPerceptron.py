# -*- coding: utf-8 -*-

import tensorflow as tf
from utils import data_convert


class MLPCpnfig(object):
    FEATURE_DIM = 40
    LABEL_DIM = 12
    HIDDEN_DIM = [400, 400, 300, 200]
    LEARNING_RATE = 0.01
    MAX_TO_KEEP_TRAIN_VERSION = 3
    NUM_EPOCHS = 100
    SAVE_EPOCH_TURN = 1
    BATCH_SIZE = 5

    CHECK_POINT_DIR = 'models'


class MLPModel(object):

    def __init__(self, mlp_config):

        self.__config = mlp_config

        # placeholder input layer Variable
        self.__input_features = tf.placeholder(dtype='float', shape=[None, self.__config.FEATURE_DIM])
        self.__input_labels = tf.placeholder(dtype='float', shape=[None, self.__config.LABEL_DIM])

        # placeholder hidden layer Variables
        hidden_layer = self.__input_features
        for index, dim in enumerate(self.__config.HIDDEN_DIM):
            if index == 0:
                previous_layer_dim = self.__config.FEATURE_DIM
            else:
                previous_layer_dim = self.__config.HIDDEN_DIM[index-1]
            weight = tf.Variable(tf.truncated_normal(shape=[previous_layer_dim, dim], stddev=0.1))
            bias = tf.Variable(tf.zeros(shape=[dim], dtype='float'))
            hidden_layer = tf.tanh(tf.nn.xw_plus_b(x=hidden_layer, weights=weight, biases=bias))

        # placeholder output layer Variable
        weight = tf.Variable(tf.truncated_normal(shape=[self.__config.HIDDEN_DIM[-1], self.__config.LABEL_DIM], stddev=0.1))
        bias = tf.Variable(tf.zeros(shape=[self.__config.LABEL_DIM], dtype='float'))
        self.__output = tf.nn.softmax(tf.nn.xw_plus_b(x=hidden_layer, weights=weight, biases=bias))

        # cost function and optimizer
        self.__cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(self.__output, self.__input_labels))
        self.__correct_prediction = tf.equal(tf.arg_max(self.__output, 1), tf.arg_max(self.__input_labels, 1))
        self.__accuracy = tf.reduce_mean(tf.cast(x=self.__correct_prediction, dtype='float'))
        optimizer = tf.train.GradientDescentOptimizer(learning_rate=self.__config.LEARNING_RATE)
        self.__train_operator = optimizer.minimize(self.__cost)

        # save session
        self.__saver = tf.train.Saver(tf.all_variables(), max_to_keep=self.__config.MAX_TO_KEEP_TRAIN_VERSION)

        # initial variables
        self.__init = tf.initialize_all_variables()

    def train(self, features, labels):
        tf.scalar_summary("accuracy", self.__accuracy)
        merged = tf.merge_all_summaries()
        print '*' * 20 + 'Train model: Starting...' + '*' * 20
        with tf.Session() as sess:
            writer = tf.train.SummaryWriter("tb_logs", sess.graph_def)
            sess.run(self.__init)
            for epoch in range(1, self.__config.NUM_EPOCHS + 1):
                batch = 0.11 + epoch / self.__config.NUM_EPOCHS
                limit_data = int(batch * len(features) * epoch / self.__config.NUM_EPOCHS)
                limit_data = min(limit_data, len(features))
                shuffled_features, shuffled_labels = data_convert.shuffle_pair_data(data=features[:limit_data], labels=labels[:limit_data])
                batched_data = data_convert.batch_pair_data(
                    features=shuffled_features,
                    labels=shuffled_labels,
                    batch_size=self.__config.BATCH_SIZE
                )
                for batch_feature, batch_label in batched_data:
                    sess.run(self.__train_operator, feed_dict={
                        self.__input_features: batch_feature,
                        self.__input_labels: batch_label
                    })
                if epoch % self.__config.SAVE_EPOCH_TURN == 0 or epoch == self.__config.NUM_EPOCHS:
                    self.__saver.save(
                        sess=sess,
                        save_path=self.__config.CHECK_POINT_DIR + str('/model'),
                        global_step=epoch//self.__config.SAVE_EPOCH_TURN
                    )
                    result_merge, check_accuracy = sess.run([merged, self.__accuracy], feed_dict={
                        self.__input_features: features,
                        self.__input_labels: labels
                    })
                    writer.add_summary(result_merge, epoch)
                    print 'Step {a} || Accuracy = {b}'.format(a=epoch, b=check_accuracy)
        print '*' * 20 + 'Train model: Completed!' + '*' * 20

    def test(self, features, labels):
        print '*' * 20 + 'Test model: Starting...' + '*' * 20
        with tf.Session() as sess:
            sess = self.create_session(sess=sess)
            check_acc = sess.run(self.__accuracy, feed_dict={
                self.__input_features: features,
                self.__input_labels: labels
            })
            print 'Test model: accuracy = ', check_acc
        print '*' * 20 + 'Test model: Completed!' + '*' * 20

    def predict_one(self, features):
        print '*' * 20 + 'Predict: Starting...' + '*' * 20
        with tf.Session() as sess:
            sess = self.create_session(sess=sess)
            res = sess.run(self.__output, feed_dict={
                self.__input_features: features
            })
            sess.close()
        return res

    def create_session(self, sess):
        ckpt = tf.train.get_checkpoint_state(self.__config.CHECK_POINT_DIR)
        if ckpt and ckpt.model_checkpoint_path:
            self.__saver.restore(sess=sess, save_path=ckpt.model_checkpoint_path)
            return sess
        else:
            return None





