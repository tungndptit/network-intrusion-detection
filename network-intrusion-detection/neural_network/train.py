#! /usr/bin/env python
# -*- coding: utf-8 -*-

import cPickle as pickle
import config
from data_process import data_loader, vocabulary
from neural_network import nn_model


def train():
    features, labels = data_loader.load_train_data()
    vob = vocabulary.Vocabulary(features, labels)

    with open(config.FilePath.VOCABULARY, 'wb') as fp:
        pickle.dump(vob, fp)
        fp.close()

    print 'Label dimension ', len(labels[0])
    features = vob.convert_features(features)
    labels = vob.convert_labels(labels)

    mlp_config = nn_model.MLPCpnfig()
    mlp_config.LABEL_DIM = len(labels[0])
    model = nn_model.MLPModel(mlp_config=mlp_config)

    feats_train, labels_train, feats_test, labels_test = data_loader.split_data(features=features, labels=labels, percent_split=0.85)

    model.train(features=feats_train, labels=labels_train)

    model.test(features=feats_test, labels=labels_test)
