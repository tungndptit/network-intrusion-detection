#! /usr/bin/env python
# -*- coding: utf-8 -*-

import os


def read_matrix_float(file_path, separator='\t'):

    features = []
    if os.path.isfile(file_path):
        fin = open(file_path, 'r')
        samples = list(fin.readlines())
        fin.close()
        for sample in samples:
            sample = sample.strip()
            # ignore the empty line
            if 0 < len(sample):
                sample_arr = sample.split(separator)
                features.append(map(float, sample_arr))
    return features


def read_matrix_float_with_label(file_path, separator='\t'):

    features = []
    labels = []
    if os.path.isfile(file_path):
        fin = open(file_path, 'r')
        samples = list(fin.readlines())
        fin.close()
        for sample in samples:
            sample = sample.strip()
            # ignore the empty line
            if 0 < len(sample):
                sample_arr = sample.split(separator)
                features.append(map(float, sample_arr[:-1]))
                labels.append(float(sample_arr[-1]))
    return features, labels
