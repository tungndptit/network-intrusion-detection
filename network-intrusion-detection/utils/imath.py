#! /usr/bin/env python
# -*- coding: utf-8 -*-

import math


def euclidean_distance(vector_a, vector_b):

    # if len(vector_a) != len(vector_b):
    #     raise Exception('2 vectors do not have same size')
    assert len(vector_a) == len(vector_b)
    dist = 0.0
    for i in range(len(vector_a)):
        dist += math.pow(vector_a[i]-vector_b[i], 2)
    return math.sqrt(dist)


def cosine_similar(vector_a, vector_b):

    # if len(vector_a) != len(vector_b):
    #     raise Exception('2 vectors do not have same size')
    assert len(vector_a) == len(vector_b)
    dot_product = 0.0
    magnitude_a = 0.0
    magnitude_b = 0.0
    for i in range(len(vector_a)):
        dot_product += vector_a[i] * vector_b[i]
        magnitude_a = math.pow(vector_a[i], 2)
        magnitude_b = math.pow(vector_b[i], 2)
    magnitude = math.sqrt(magnitude_a * magnitude_b)
    if magnitude == 0:
        return 0.0
    return 1.0 * dot_product / magnitude


def swap(a, b):
    return b, a


def gcd(a, b):

    if a == 0 or b == 0:
        return 0
    assert a > 0 and b > 0
    while 0 < b:
        a, b = swap(a % b, b)
    return a


def lcd(a, b):

    _gcd = gcd(a, b)
    return a * b / _gcd


def get_max_bit(number):
    count = 0
    while 0 < number:
        count += 1
        number >>= 1
    return count


def two_list_is_same(list_a, list_b):
    if len(list_a) != len(list_b):
        return False
    for a, b in zip(list_a, list_b):
        if a != b:
            return False
    return True

