#! /usr/bin/env python
# -*- coding: utf-8 -*-

import random
import numpy as np


def batch_data(data, batch_size):
    left = 0
    for i in range(batch_size, len(data), batch_size):
        right = min(len(data), i)
        yield data[left:right]
        left = right
    if right < len(data)-1:
        yield data[right:len(data)]


def batch_pair_data(features, labels, batch_size):
    assert len(features) == len(labels)
    if len(features) < batch_size:
        yield (features, labels)
    else:
        left = 0
        length = len(features)
        for i in range(batch_size, length, batch_size):
            right = min(length, i)
            yield (features[left:right], labels[left:right])
            left = right
        if right < length-1:
            yield (features[right:length], labels[right:length])


def shuffle_pair_data(data, labels):
    shuffle_indices = range(len(data))
    random.shuffle(shuffle_indices)
    data_shuffled = np.array(data)[shuffle_indices]
    target_shuffled = np.array(labels)[shuffle_indices]
    return data_shuffled, target_shuffled

