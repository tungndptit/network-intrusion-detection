#! /usr/bin/env python
# -*- coding: utf-8 -*-

import re


class TConstants(object):
    ENCODE_TYPE = 'utf-8'
    DECODE_TYPE = 'utf8'
    GENERAL_NODE = u'general'


def str_encode(string):
    try:
        string = string.encode(encoding=TConstants.ENCODE_TYPE)
    except:
        pass
    return string


def str_decode(string):
    try:
        string = string.decode(encoding=TConstants.ENCODE_TYPE)
    except:
        pass
    return string


def clean_str(string):
    string = str_decode(string).lower()
    string = str_encode(string)
    string = re.sub(r"\(([0-9a-zA-Z\s\.,]+)\)", " ", string)
    string = re.sub(r"-", " - ", string)
    string = re.sub(r"–", " - ", string)
    # string = re.sub(r"\xe2\x80\x93", " ", string)
    string = re.sub(r",(?!\d)", " , ", string)
    # string = re.sub(r'"', ' " ', string)
    string = re.sub(r'"', ' " ', string)
    string = re.sub(r"''", ' " ', string)
    string = re.sub(r'\.(?!\d)', ' . ', string)
    string = re.sub(r"!", " ! ", string)
    string = re.sub(r"/", " / ", string)
    string = re.sub(r";", " ; ", string)
    string = re.sub(r"&", " & ", string)
    # string = re.sub(r":", " : ", string)
    string = re.sub(r":", " ", string)
    string = re.sub(r"\+", " + ", string)
    string = re.sub(r"\(", " ( ", string)
    string = re.sub(r"\)", " ) ", string)
    string = re.sub(r"\?", " ? ", string)
    string_arr = re.split('([.\d]+)', string.strip())
    string = ' '.join(string_arr)
    string = re.sub('\s+', ' ', string)
    return string

