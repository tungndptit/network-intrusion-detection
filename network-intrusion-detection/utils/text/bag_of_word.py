#! /usr/bin/env python
# -*- coding: utf-8 -*-


def bag_word_from_2d_list(lines, keep_frequency=True):
    word_count = {}
    for line in lines:
        words = str(line).split()
        for word in words:
            if word in word_count:
                word_count[word] += 1
            else:
                word_count[word] = 1
    return __word_count_to_dict(word_count, keep_frequency=keep_frequency)


def __word_count_to_dict(word_count, keep_frequency):

    word_count_sorted = sorted(word_count.items(), key=lambda x: x[1], reverse=True)
    for index, wcs in enumerate(word_count_sorted):
        if keep_frequency:
            word_count[wcs[0]] = tuple([index+1, wcs[1]])
        else:
            word_count[wcs[0]] = tuple([index+1])
    return word_count


def text_to_id_array(line, text_dict):
    words = line.split()
    word_ids = [0] * len(words)
    for i, word in enumerate(words):
        if word in text_dict:
            word_ids[i] = text_dict[word][0]
    return word_ids

"""
# TEST
test_dict = [
    'ca la loai dong vat song duoi nuoc',
    'co nhieu loai ca ca to ca be'
]
text_dict = bag_word_from_2D_list(test_dict, True)
print text_dict
test_line = 'ca to chua chac da ngon hon ca be'
# print text_to_id_array(test_line, text_dict)
"""
