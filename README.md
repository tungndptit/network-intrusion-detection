# **Network Intrusion Detection**

#### * Requirements:
- Python 2.7
- Tensorflow version 0.7 (python API only)

#### * Data using:
- KDD Cup 1999 Data

#### * How to train data
- First step: create folder (if not exist)
    - Folder: **_models_** (to save models)
    - Folder: **_tb_logs_** (to save tensorboard)
    - Folder: **_data_** (to save input data)
- Second step: down load data
    - Link: http://kdd.ics.uci.edu/databases/kddcup99/kddcup99.html
    - Download file: **_kddcup.data.gz The full data set (18M; 743M Uncompressed)_**
    - Extract downloaded file and copy **_kddcup.data.corrected_** into **_data_** folder
    - Keep file name as **_kddcup.data.corrected_** 
- Third step:
    - Run: **_main_train_apps_**
    - Syntax: **_~$ python main_train_apps.py_** (on terminal)

#### * How to predict data
- First step: train data
- Second step: create input file 
    - File path: **_~/data/predict.txt_**
    - Put sample which you want to predict (sample do not have label)
- Third step:
    - Run: **_main_predict_apps_**
    - Syntax: **_~$ python main_predict_apps.py_** (on terminal)
    
#### * Tensorboard
- How to view tensorboard   
    - Run tensorboard service
    - Syntax: **_tensorboard --logdir=tb_logs_** (on terminal)
    - Go to: http://0.0.0.0:6006