#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
" @author Tung, Nguyen-Duy <tungnd.ptit@gmail.com>
" @date   06/09/2016
"
"""
from setuptools import setup

setup(
    name='network-intrusion-detection',
    version='1.0',
    long_description='',
    url='',
    author='Tung Nguyen-Duy',
    license='',
    install_requires=['tensorflow', 'numpy'],
    packages=[
        'network-intrusion-detection',
        'network-intrusion-detection/data_process',
        'network-intrusion-detection/neural_network',
        'network-intrusion-detection/utils'
    ]
)